# EPFL CS230 - Computer Language Processing (2024)

When and where:

- Lectures are on Wednesday 13:15 - 15:00 in [BC01](https://plan.epfl.ch/?room==BC%2001)
- Labs and exercises are on Friday 13:15 - 17:00 in [ELA2](https://plan.epfl.ch/?room==ELA%202)

We won't check for anyone's attendance at the class, labs, or exercises.
Further, we will record lectures happening on Wednesdays and host them in this repository.
Nonetheless, note that additional information about the contents of the course may be delivered in any of the slots above.
It is also possible that we ask students to be present on a particular date for a quizz or presentation.

While we will try to communicate as transparently and as early as possible on any outstanding event, keep this information in mind if these slots conflict with another class or if you simply intend to skip exercise/labs sessions.

Who:

- [Dimi Racordon](https://people.epfl.ch/dimitri.racordon) (she/her): Lecturer
- [Sankalp Gambhir](https://people.epfl.ch/sankalp.gambhir) (he/him): Teaching assistant
- [Samuel Chassot](https://people.epfl.ch/samuel.chassot?lang=en) (he/him): Teaching assistant, he/him

## Grading

The grade is based on a midterm (30%), labs (30%), and a final project (40%).
The latter will be evaluated on its implementation in Scala, a written report, and an oral presentation.
There will be no final written exam.

Here is the weight of the milestones in the overall course grade:

- 30% Midterm (date 17.04.24)
- 5% Lab 1
- 5% Lab 2
- 5% Lab 3
- 5% Lab 4
- 5% Lab 5
- 5% Lab 6
- 40% Project (including final presentation)

## Material

The course is designed to be self-contained.
Nonetheless, the following books contain overlapping material:


- [Basics of Compiler Design](http://hjemmesider.diku.dk/~torbenm/Basics/). Pages 9-88 (omit Section 2.8) for lexical analysis and parsing
- [Modern compiler implementation in ML](http://library.epfl.ch/en/beast?isbn=9781107266391). Sections 2.1-2.4 for Lexical analysis, Sections 3.1-3.2 for parsing, and 5.3-5.4 as well as 16.1-16.3 for type checking
- [Compilers, principle, techniques and tools](http://library.epfl.ch/en/beast?isbn=9781292024349)

## Labs and final project

The first six weeks of the semester will feature a series of six lab sessions.
During these labs, you will implement all stages of a compiler pipeline for a new programming language: Alpine.

Deadlines for labs and project are shown in the following table. They are also available in each lab's assignment on Moodle.

| Lab | Release | Deadline (12am) |
| --- | --- | --- |
| Lab01: Interpreter | 21.02.2024 | 01.03.2024 |
| Lab02: Parser | 28.02.2024 | 15.03.2024 |
| Lab03 + Lab04: Type Checker + Inference| ~08.03.2024 | 28.03.2024 |
| Lab05: Code Gen | 27.03.2024 | 12.04.2024 |
| Lab06: Optimisations | latest 10.04.2024 | 19.04.2024 |
| Project proposal | 27.03.2024 | 19.04.2024 |
| Project code + report | - | 31.05.2024 |
| Project presentation | - | 27.05.2024 → 31.05.2024 |

The first lab assignment is exceptionally hidden until after the group formation deadline to avoid issues. For the following labs, the assignment (and therefore the grader) will be opened at the same time as the lab's release.

Details about the project will follow.

Following the labs, you will work on a project where you will choose and implement additional feature(s) for the compiler.
A list of features will be proposed later but you may also come up with your own suggestions.
Details will follow soon.

### Grading

The first six labs will be evaluated automatically via Moodle.
You will submit your assignments (specific instructions in each lab's handout) and receive your grade directly on Moodle after.
The grading process is based on the tests provided within the lab materials.
No hidden tests will be used.

The number of submissions is unlimited, you are free to submit as many times as you like.

We encourage frequent submissions to receive timely feedback.
Submit your work early, even if it's incomplete, to avoid any last-minute issues.

As you will be part of a Moodle group, only one member has to submit. The submission will show up (as well as the grade and feedback) to all members of the group regardless who submitted.

**Please note, submissions after the deadline will not be accepted!**

### Group work

Both the labs and the project are to be completed in groups of 2 or 3 members. Groups may be formed according to your own preferences but shall not change throughout the semester.

Please register your group on Moodle by **Sunday, February 25th, at 8 PM** (under "Group Formation").
Students not registered by this deadline will be randomly assigned to groups.

When registering, please select a group of your preferred group size.
Note that registering in a group that's not yet full indicates that you are open to having additional member(s) join.

We will assume that every member of each group contributed equally to the submitted work.
In case of issues, get in touch with us **before** the deadline via email to both [Dimi Racordon](mailto:dimitri.racordon@epfl.ch) and [Samuel Chassot](samuel.chassot@epfl.ch) explaining the problem.
Solutions will be discussed on a case-by-case basis.

### Ed Forum

We use Ed as a forum for questions and discussions.
The link is available on the course's Moodle page.
Do not hesitate to ask questions, start discussions, and answer question
