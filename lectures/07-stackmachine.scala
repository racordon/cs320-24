/** An instruction of the stack-based machine. */
enum Instruction:

  /** Pushes a constant value. */
  case cst(value: Int)

  /** Assigns `id` to the top operand. */
  case set(id: Int)

  /** Pushes the value assigned to `id`. */
  case get(id: Int)

  /** Pushes the product of `H~` and `H`. */
  case mul

  /** Pushes the result of `H` subtracted from `H~`. */
  case sub

  /** Compares `H~` and `H`. */
  case cmp

  /** Pushes two copies of `H`. */
  case cpy

  /** Pops `H`. */
  case pop

  /** Exchange `H~` with `H`. */
  case swap

  /** Unconditionally jumps to `offset`. */
  case j(offset: Int)

  /** Jumps to `offset` iff the machine status is less than or equal. */
  case jle(offset: Int)

  /** Jumps to `H~` and pushes a copy of `H`. */
  case ret

end Instruction

/** A stack-based virtual machine. */
object Machine:

  /** Returns the result of a three-way comparison between `lhs` and `rhs`. */
  private def cmp(lhs: Int, rhs: Int): Int =
    if (lhs < rhs) then -1 else if (lhs > rhs) then 1 else 0

  /** Evaluates `program` or stop after `maxSteps` steps. */
  def eval(program: IArray[Instruction], maxSteps: Int = 100): List[Int] =
    /** The program counter. */
    var pc = 0

    /** The status flag. */
    var status = 0

    /** The stack. */
    var stack = List[Int]()

    /** The registers. */
    var registers = Map[Int, Int]()

    /** The number of steps still allowed. */
    var fuel = maxSteps

    /** Applies `s` and increments the program counter. */
    def advance(s: => Unit): Unit =
      s; pc += 1

    /** Pushes `v`. */
    def push(v: Int): Unit =
      stack = v :: stack

    /** Returns the value of `H`, */
    def pop(): Int =
      val head :: tail = stack: @unchecked
      stack = tail
      head

    while (pc < program.length) && (fuel >= 0) do
      println(s"${pc} ${stack}")
      fuel -= 1

      import Instruction as S
      program(pc) match
        case S.cst(v) =>
          advance { push(v) }
        case S.set(x) =>
          advance { registers = registers + (x -> pop()) }
        case S.get(x) =>
          advance { push(registers(x)) }
        case S.mul =>
          advance { push(pop() * pop()) }
        case S.sub =>
          advance { val rhs = pop(); push(pop() - rhs) }
        case S.cmp =>
          advance { val rhs = pop(); status = cmp(pop(), rhs) }
        case S.cpy =>
          advance { push(stack.head) }
        case S.pop =>
          advance { pop() }
        case S.swap =>
          advance { val x = pop(); val y = pop(); stack = List(y, x) ++ stack }
        case S.j(o) =>
          pc = o
        case S.jle(o) =>
          pc = if (status <= 0) then o else pc + 1
        case S.ret =>
          val r = pop()
          pc = if stack.isEmpty then program.length else pop()
          push(r)

    stack

end Machine

val program: IArray[Instruction] =
  import Instruction.*
  IArray[Instruction](
    cst(4),  // 0
    cpy,     // 1
    cst(1),  // 2
    cmp,     // 3
    jle(13), // 4
    cpy,     // 5
    cst(11), // 6
    swap,    // 7
    cst(1),  // 8
    sub,     // 9
    j(1),    // 10
    mul,     // 11
    j(15),   // 12
    pop,     // 13
    cst(1),  // 14
    ret,     // 15
  )

@main def run() =
  print(Machine.eval(program))
