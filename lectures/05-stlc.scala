import java.util.UUID

/** A type error. */
final class TypeError extends Exception

/** A term of the simply typed lambda calculus. */
trait Term:

  /** Returns the set of variables that occur free in `this`. */
  def free: Set[Variable] =
    this match
      case x: Variable => Set(x)
      case Application(f, a) => f.free.union(a.free)
      case Abstraction(x, _, e) => e.free.excl(x)

  /** Applies the capture-avoiding substitution of `e` for `x` in `this`. */
  def replace(x: Variable, e: Term): Term =
    this match
      case y: Variable => if (x == y) then e else this
      case Application(f, a) =>
        Application(f.replace(x, e), a.replace(x, e))
      case Abstraction(y, t, ee) =>
        if x == y then this else
          val r = if e.free.contains(y) then
            e.replace(y, Variable.fresh())
          else
            e
          Abstraction(y, t, ee.replace(x, r))

  /** Evaluates `this` to a normal form or diverges. */
  def evaluate(): Term =
    this match
      case Application(f, a) =>
        val (u, v) = (f.evaluate(), a.evaluate())
        u match
          case Abstraction(x, _, e) => e.replace(x, v)
          case u => Application(u, v)
      case _ => this

  /** Returns the type of `this` given the typing environment `g`,
   *  throwing an exception if `this` is ill-typed.
   */
  def checkedType(g: Map[Variable, Type]): Type =
    this match
      case x: Variable => g.get(x).get
      case Application(f, a) =>
        f.checkedType(g) match
          case Arrow(t, s) if t == a.checkedType(g) => s
          case _ => throw TypeError()
      case Abstraction(x, t, e) =>
        Arrow(t, e.checkedType(g + (x -> t)))

end Term

/** A variable. */
final case class Variable(name: String) extends Term:

  override def toString: String = name

object Variable:

  def fresh(): Variable = Variable(UUID.randomUUID.toString)

end Variable

/** The application of an abstraction. */
final case class Application(function: Term, argument: Term) extends Term:

  override def toString: String = s"(${function} ${argument})"

end Application

/** An abstraction (i.e., a function). */
final case class Abstraction(parameter: Variable, tpe: Type, body: Term) extends Term:

  override def toString: String = s"(λ${parameter}.${body})"

end Abstraction

/** A type term of the simply typed lambda calculus. */
trait Type

/** A type variable. */
final case class TypeVariable(name: String) extends Type

/** The type of an abstraction from `lhs` to `rhs`. */
final case class Arrow(lhs: Type, rhs: Type) extends Type

@main def m =
  val x = Variable("x")
  val y = Variable("y")
  val e = Application(
    Abstraction(x, TypeVariable("X"), Abstraction(y, TypeVariable("Y"), x)), y)

  println(e.checkedType(Map((y -> TypeVariable("X")))))
  println(e.evaluate())
