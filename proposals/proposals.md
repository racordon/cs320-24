# Project proposals for Alpine extensions

## [1] Open unions (1 point)

In the last lab, you have implemented code generation for a subset of Alpine.
This subset has been defined to exclude polymorphism from the language, meaning that a concrete type (e.g., `Int`) must be assigned to each expression at compile-tme.
The goal of this extension is to lift part of this restriction to support open unions, so that one can, for example, write the following code:

```swift
let x: Any = 0
let y = match x {
  case let u: Int then u
  case _ then 0
}
```

To implement this extension, you will need a way to represent an *existential container* in your target language.

Recall that we discussed a general recipe to describe such a data structure in the course.
In a nutshell, you will need a data structure capable of holding a pointer to some arbitrary data along with the information necessary for the program to understand what this data represents at run-time.

```c
typedef struct {
  void* payload; // pointer to arbitrary data
  Type* type;    // description of the contents stored at `payload`
} Any;
```

You will also have to implement pattern matching on the contents of an existential container to support programs such as the one written above.

## [2] Closed unions (1 point)

In the last lab, you have implemented code generation for a subset of Alpine.
This subset has been defined to exclude polymorphism from the language, meaning that a concrete type (e.g., `Int`) must be assigned to each expression at compile-tme.
The goal of this extension is to lift part of this restriction to support closed unions, so that one can, for example, write the following code:

```swift
let x: Int | String = 0
let y = match x {
  case let u: Int then u
  case _ then 0
}
```

To implement this extension, you will need a way to represent a *discriminated union* in your target language.

Recall that we discussed a general recipe to describe such a data structure in the course.
In a nutshell, you will need a data structure capable of holding the representation of any case in the union along with the information necessary for the program to understand what case is "active" at run-time.

```c
typedef struct {
  union { int32_t, const char* } payload;
  int8_t tag;
} IntOrString;
```

You will also have to implement pattern matching on the contents of a discriminated union to support programs such as the one written above.

## [3] Higher-order functions (1 point)

In the last lab, you have implemented code generation for a subset of Alpine.
This subset has been defined to exclude higher-order functions from the language, meaning that one cannot use functions as first-class citizen.
The goal of this extension is to lift part of this restriction to support lambdas, so that one can, for example, write the following code:

```swift
fun curry(_ f: (Int, Int) -> Int) -> (Int) -> (Int) -> Int {
  (_ x: Int) { (_ y: Int) { f(x, y) } }
}

let main = print(curry((_ a: Int, _ b: Int) { a + b })(2)(3))
```

To implement this extension, you will need a way to represent a *lambdas* in your target language.

Recall that we discussed a general recipe to describe such a data structure in the course.
In a nutshell, you will need a data structure capable of holding a pointer to the function representing the computation carried out by the lambda along with the environment of that lambda..

```c
typedef struct {
  int32_t(*f)(int32_t, int32_t);
  void* environment;
} Lambda;
```

You will also have to implement calls to lambdas.

## [4] Methods (1 point)

The goal of this extension is to support the definition of methods which can be selected using dot syntax.

All functions in Alpine are called *free functions* because they are not bound to any particular value.
A method differs in that it typically has a *receiver*, which is a special value acting as an implicit argument to the function.
The term comes from early litterature on object-oriented programming languages, where methods would represent messages *received* by an object.

One way to understand a method at a low-level, therefore, is as a free function that simply has a parameter describing the receiver:

```swift
/// This function can be seen as a method of `Int`.
fun negate(_ self: Int) -> Int { -self }
```

Free functions are not less expressive than methods but they can make composition harder to use.
For example, the expression `a.insert(1).insert(2)` is arguably easier to read than `insert(insert(a, 2), 1)`.

To bring methods to Alpine, you will first need a way to define one.
One possible approach is to allow function identifiers to be prefixed by the type of the method's receiver.
For example, the following declaration would be a mere sugar for the one we discussed above:

```swift
fun Int.negate() -> Int { -self }
```

Then you will need to modify type checking and code generation to support method calls.

## [5] Dynamic dispatch (1 point)

_Requires [2] and [4]_

The goal of this extension is to support method calls on closed unions so that one can, for example, write the following code:

```swift
fun #a.foo() -> Int { 1 }
fun #b.foo() -> Int { 2 }

let x: #a | #b = #a
let main = print(x.foo())
```

To support dynamic dispatch, you will first need a way to "convince" the typer that a method call of the form `x.f(a1, ..., a2)` is indeed valid when `x` is a closed union.
Then, you will need to adapt code generation so that a closed union contains a witness table that can be used at run-time to determine which function should be called depending on the type of the receiver.

## [6] C (2 points)

The goal of this extension is to use C as a compiler target instead of WebAssembly.

C is sometimes called the "lingua franca" of software because most systems are capable of interoperating with C to some extent.
As such, it represents an interesting compiler target because it allows a component written in language A to be reused in a larger piece of software written in language B.

You have already implemented a transpiler from Alpine to Scala.
This extension is similar to the transpiler you have written from Alpine to Scala, except that the target sits at a much lower level of abstraction.
This expressiveness gap will create a couple of challenges to overcome, in particular:

- Compile pattern matching
- Compile polymorphic containters (if you also implement [1] and/or [2])
- Compile higher-order functions (if you also implement [3])

## [7] LLVM (2 points)

The goal of this extension is to use LLVM as a compiler target instead of WebAssembly.

LLVM is a large compiler infrastructure that is in the backend of several industry-strenght programming languages, such as C, C++, Swift and Rust.
LLVM is language-agnostic, just like WebAssembly.
It has an input language that can be used as the target of any compiler front-end.
This language, called LLVM IR, is an assembly-like instruction set for an abstract machine with an infinite number of registers.

While WebAssembly is a great choice for running code on the Web or in isolated containers, LLVM comes with a few advantages:
- LLVM gives compiler writers much more freedom to lower their abstraction.
- LLVM is typically used to generate native code, which is more efficient than WebAssembly.
- LLVM can be use to target many different back-ends, including WebAssembly!

## [8] Garbage collection (1 points)

_Requires [1] or [3]_

The goal of this extension is to implement a garbage collector to dispose the memory allocated for type erasure.

One must (typically) allocate memory outside of the stack at run-time to perform the type erasure necessary for representing an existential container or the environment of a higher-order function.
Such memory should eventually be reclaimed, otherwise long running programs will eventually run out of memory.

There are several ways to implement garbage collection.
A simple approach is *reference counting*:
A counter is incremented every time a reference is made on data allocated on the store and decremented every time a reference to that same data goes out of scope.
When the counter reaches 0, the memory allocated for the associated data is freed.

For example, consider the following program:

```swift
fun make() -> Any { #a }
let main =
  let a = make() { let b = a { print(b) } }
```

When `make` is called, a new existential container is created, thus requiring some memory on the store.
When `a` is assigned, an counter associated to the container's memory is incremented to 1.
It is incremented once again when `b` is initialized, meaning that its value is `2` in the scope of `b`.
The counter is decremented when `b` goes out of scope and decremented again when `a` does the same.
At that point its value reaches 0 and the payload of the existential container gets deallocated.

## [9] Concurrency (3 points)

The goal of this extension is to add a concurrent primitive to Alpine so that one can, for example, write the following program:

```swift
let main = join(print("a"), print("b"))
```

Because the two expressions of the join should run concurrently, the result of standard output is nondeterministic.
In one case it may be "ab" and in the other it may be "ba".

To implement this extension, you will have to define `join` as a new primitive of the language that accepts two expressions and run them concurrently.
The result of a call to `join` is a record `#join(lhs: T, rhs: U)` where `T` and `U` are the types of the expressions passed to `join`.

## [10] Exceptions (3 points)

The goal of this extension is to support exceptions so that one can, for example, write the following program:

```swift
fun foo() { throw(#error) }
let main = try foo() catch(let e) print(e)
```

In this example, the program is calling `foo`, which throws an exception that is then caught, assigned to `e`, and then printed.

To implement this extension, you will first have to define two new constructs in Alpine:
- A `throw(e)` primitive that accepts any value `e` and throws it as an exception
- A `try e catch(p) h` that runs `e` and catches potential exceptions using pattern `p` and handler `h`.


Then, you will have to implement a mechanism to let functions return with an exception.
There are several ways to achieve that.
One simple approach is to have all functions accept an additional implicit parameter that is a pointer to some memory location capable of holding an exception.
Nothing is placed at that location when a function returns normally (i.e., without throwing).
If an exception is thrown, however, it is written to that location and the function returns immediately.
When a function returns, the caller can check if an exception has been written at that location.

## [11] Mutation (3 points)

Alpine is a pure function language, meaning that all variables are immutable.
The goal of this extension is to lift this restriction to support mutable local bindings.
For example:

```swift
let main = var x = 2 {
  print(x); // prints 2
  x = x + 1;
  print(x) // prints 4
}
```

In this example, `x` is a local binding introduced with `var` instead of `x`, allowing it to be reassigned in its scope.

Mutation asks thorny questions regarding semantics.
For the purpose of this extension, the objective is to implement mutable value semantics: all values must behave like so-called primitive data types (e.g., `Int`).

To implement this feature, you will have to modify the declaration of local bindings so they support either the `let` or `var` introducer.
You will also need an additional AST node to represent sequences of expressions, as it should be possible to observe the effect of an assignment.
Just like in Scala, the value of a sequence should be the result of the last expression in that sequence.
