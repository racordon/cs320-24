% preliminaries.tex
% preliminaries about DFA / NFA

The next section defines the notation of words, languages, automata, and regular
expressions as it is used in the remainder of the exercises. It is merely
intended to be used as a compact reference for the reader.

\section*{Syntax Review}

The symbols \(\lnot, \land, \lor, \implies, \iff\) represent the usual notions
of Boolean negation, conjunction, disjunction, implication, and equivalence,
respectively. The set of natural numbers \(\{0, 1, 2, \ldots\}\) is denoted
\(\naturals\).

For a set of symbols \(\Sigma\), a finite sequence of symbols from \(\Sigma\) is
called a word over \(\Sigma\). \(\Sigma^*\) is set of all (possibly empty) words
over \(\Sigma\). 
%
For a word \(w\), \(|w|\) denotes its \emph{length}.
%
The empty word (of length 0) is denoted \(\epsilon\). 
%
\(w_i\) for some \(i < |w| \in \naturals\) is the symbol at the \(i\)th position
in \(w\).
%
Any set of words \(L \subseteq \Sigma^*\) is called a language over \(\Sigma\).


A \emph{non-deterministic finite automaton} (NFA) is a 5-tuple \((Q, \Sigma,
\delta, S, F)\), where \(Q\) is a finite set of states, \(\Sigma\) a finite set
of symbols called the \emph{alphabet}, \(\delta \subseteq Q \times \Sigma \times
Q\) is a \emph{transition relation}, \(S \subseteq Q\) the set of \emph{initial
states}, and \(F\) the set of \emph{final} or \emph{accepting states}. 

A word \(w\) is \emph{accepted} by an automaton \(A = (Q, \Sigma, \delta, S,
F)\) iff there exists a sequence of states \(s\) with \(|s| = |w| + 1, s_0 \in
S, s_{|s|} \in F\), and \(\forall\; i < |s| - 1.\; \delta(s_i, w_i, s_{i+1})\).
The set of all words accepted by an automaton \(A\) is called its
\emph{language}, and denoted \(L(A)\).

A language \(L\) is called \(regular\) as a set or language iff there exists an
automaton \(A\) such that \(L(A) = L\).

An NFA is also a \emph{deterministic finite automaton} (DFA) if it has a single
initial state, \(|S| = 1\), and its transition relation \(\delta\) is functional,
i.e.
%
\begin{gather*}
    \forall\; q \in Q, a \in \Sigma.\; \exists!\; q' \in Q.\; \delta(q, a, q')~.
\end{gather*}

We may sometimes abuse notation and omit paths that do not reach any final state
(in which case the transition relation is not visibly functional). These are
also called `dead' states or paths.

On languages, we define binary operations \(\cup, \cap,\) and \(\cdot\), where
\(\cup\) and \(\cap\) correspond to the usual union and intersection of sets,
and \(\cdot\) is the \emph{concatenation} of words lifted to sets defined by:
%
\begin{gather*}
    \forall\; w_1, w_2 \in \Sigma^*.\; (w_1 \cdot w_2) (i) = \begin{cases}
        w_1(i) \text{ if } i < |w_1| \\
        w_2(i - |w_1|) \text{ if } i \geq |w_1| \\
    \end{cases} \\
    \forall\; L_1, L_2 \subseteq \Sigma^*.\; L_1 \cdot L_2 = \{w \in \Sigma^* \mid \exists\; w_1 \in L_1, w_2 \in L_2.\; w_1 \cdot w_2  = w\}
\end{gather*}

We also define the unary operations \(\bar L\), \(L^i\) and \(L^*\), the first
being the usual complementation on sets with respect to \(\Sigma^*\), the second
being exponentiation (interpreting \(\cdot\) as multiplication) and \(L^*\)
being the \emph{Kleene star}:
%
\begin{gather*}
    \forall\; w \in \Sigma^*.\; w^* = \{w^i | i \in \naturals\} \\
    \forall\; L \subseteq \Sigma^*.\; L^* = \bigcup_i L^i
\end{gather*}

The Kleene star can also be extended to the operation \(L^+ = L \cdot L*\), the
set of all non-empty words in \(L^*\).

A \emph{regular expression} over \(\Sigma\) is a term built from the following
context-free grammar:
%
\begin{gather*}
    reg := (reg ~|~ reg) \mid (reg \cdot reg) \mid reg^* \mid \Sigma
\end{gather*}

To each regular expression, we recursively associate a language, given by
%
\begin{gather*}
    L(a) = \{a\} \text{ for } a \in \Sigma \\
    L(reg_1 \mid reg_2) = L(reg_1) \cup L(reg_2) \\  
    L(reg_1 \cdot reg_2) = L(reg_1) \cdot L(reg_2) \\
    L(reg^*) = L(reg)^*  
\end{gather*}
%
and we degenerately refer to the language associated to a regular expression by
the expression itself, unless context requires clarification.

A \emph{grammar} is a 4-tuple \(G = (N, T, P, S)\), where \(N\) is a finite set
of `non-terminal' symbols, \(T\) a finite set of `terminal' symbols disjoint
from \(N\), \(P\) a finite set of `productions' or `rules' \(\subseteq (N \cup
T)^+ \times (N \cup T)^*\), and \(S \in N\) the `start' symbol. 
%
% If \(T = \Sigma \cup \{\epsilon\}\) for some finite alphabet \(\Sigma\), then we
% say \(G\) is a grammar \emph{over} \(\Sigma\).  

We write the production rules as rewrites \(a \to b\) instead of pairs \((a, b)
\in P\).

If for every production rule in \(P\), \(\exists\; S\in N.\;\) such that the
rule is of the form \(S \to w\) with \(w \in (N \cup T)^*\), i.e., only has one
non-terminal on the left side, then we say \(G\) is a context-free grammar
(CFG).
