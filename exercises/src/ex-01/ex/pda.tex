% pda.tex
% problems over pdas

\subsection*{Pushdown Automata}

%%%
In class, we saw a way to recognize context-free languages by reading off
the word and pushing and popping symbols from a stack. This idea is
formalized by the notion of a \emph{pushdown automaton}.

A \emph{pushdown automaton} (PDA) is a 7-tuple \((Q, \Sigma, \Gamma, \delta,
q_0, Z_0, F)\), where \(Q\) is a finite set of states, \(\Sigma\) a finite
set of input symbols, \(\Gamma\) a finite set of stack symbols, \(\delta
\subseteq Q \times (\Sigma \cup \{\epsilon\}) \times \Gamma \times Q \times
\Gamma^*\) a transition relation, \(q_0 \in Q\) the initial state, \(Z_0 \in
\Gamma\) the initial stack symbol, and \(F \subseteq Q\) the set of final
states.

Intuitively, a PDA is an extension of an NFA with a stack. Each transition
reads one or zero symbols from the input string, popping a symbol from the
stack at each step. Then, it takes a `transition' based on the combination
of the input it read and the symbol it popped, moving to a new state, and
ending by pushing zero or more symbols onto the stack. 

The symbols pushed onto the stack represent the `remaining computation'.
This motivates the definition of acceptance, wherein there is no computation
remaining, and we reach a final state:

Informally, a word \(w\) is \emph{accepted} by a PDA \(P\) as above if,
starting in the state \(q_0\) with the stack containing only \(Z_0\), we can
reach a final state \(q_f \in F\) following transitions in \(\delta\) after
reading the entire word \(w\), and emptying the stack.

As an example, let us construct a PDA for the grammar \(G\) defined in the
previous exercise (balanced parentheses).

First, we transform the CFG into Greibach normal form. The transformation is
straightforward; it involves replacing each production \(A \to wBw'\) with
\(A \to wBC\) and adding \(C \to w'\) as a rule, where \(A\), \(B\), and
\(C\) are non-terminal symbols, and \(w\) and \(w'\) are words over
\(\Sigma\) (of terminal symbols). We are ensuring that each transition rule
is either terminal, or ends in non-terminal symbols only. 

Doing this for the grammar \(G\), we get the following rules:
\begin{align*}
    S &\to aSB \mid SS \mid \epsilon \\
    B &\to b
\end{align*}
%

Now, we can construct a PDA for the language \(L(G)\). Define the PDA
\begin{gather*}
    P = (\{q_0, q_1, q_f\}, \Sigma \cup \{\epsilon\}, \Gamma, \delta, q_0, Z, \{q_f\})~.
\end{gather*}

There are only three states. An initial state, \(q_0\), to initialize the
stack, an accepting state \(q_f\), and a state \(q_1\) where all our
computation occurs. The symbol \(Z\) is the specially chosen initial stack
symbol, disjoint from the symbols in the grammar.

We use the notation \(\langle a, s, w\rangle\) on a transition to mean that the
automaton reads an input symbol \(a \in \Sigma \cup \{\epsilon\}\), pops the
stack symbol \(s \in \Gamma\), and pushes the word \(w \in \Gamma^*\) onto the
stack. If we say \(\langle a, s, abcd\rangle\), the symbols are pushed onto the
stack in the order \(d, c, b, a\), leaving \(a\) at the top.

To define the transition relation \(\delta\), we follow three steps:
\begin{enumerate}
    \item \(q_0 \xrightarrow{\langle \epsilon, Z, SZ\rangle} q_1\): reading
    the initial symbol, initializing the stack with the starting symbol
    \(S\). We keep \(Z\) on the stack to recognize when the computation is
    finished.
    \item \(q_1 \xrightarrow{\langle a, T, w\rangle} q_1\): for every
    production rule \(T \to aw\) with \(T \in \Gamma\), \(a \in \Sigma \cup
    \{\epsilon\}\), and \(w \in \Gamma^*\), we add a transition that reads
    \(a\) from the input. This corresponds to reading a non-terminal \(T\)
    from the stack and incrementally replacing it by its expansion.
    \item Finally, when the input is done, we can read \(Z\) from the stack
    and move to the final state: \(q_1 \xrightarrow{\langle \epsilon, Z,
    \epsilon \rangle} q_f\).
\end{enumerate}

This completes the construction. Note that we only need these simple states
as all the memory has been offloaded to the stack!

Visually, for this grammar:
%
\begin{center}
    \begin{tikzpicture}[node distance = 3cm, on grid]
        \node[state, initial] (q0) {\(q_0\)};
        \node[state, right=of q0, minimum size = 1.5cm] (q1) {\(q_1\)};
        \node[state, accepting, right=of q1] (qf) {\(q_f\)};

        \draw[->] (q0) edge node[above] {\scriptsize \(\langle \epsilon, Z, SZ\rangle\)} (q1);
        \draw[->] (q1) edge node[above] {\scriptsize \(\langle \epsilon, Z, \epsilon\rangle\)} (qf);

        \draw[->, out=45, in=135, loop] (q1.north) to node[above] {\scriptsize \(\langle a, S, SB\rangle\)} (q1.north);
        \draw[->, out=-90, in=0, loop] (q1.315) to node[below right] {\scriptsize \(\langle \epsilon, S, SS\rangle\)} (q1.315);
        \draw[->, out=180, in=270, loop] (q1.225) to node[below left] {\scriptsize \(\langle \epsilon, S, \epsilon\rangle\)} (q1.225);
        \draw[->, out=225, in=315, loop] (q1.270) to node[below] {\scriptsize \(\langle b, B, \epsilon\rangle\)} (q1.270);
    \end{tikzpicture}
\end{center}

The four self-loops on \(q_1\) directly correspond to the four possible
rules in our grammar.

The construction is adapted and simplified from Section 7.2 of Peter Linz's An
Introduction to Formal Languages and Automata \cite{linz2022introduction}, where
more details and proofs can be found.


\begin{exercise}{\easy}
    For the PDA \(P\) constructed above:

    \begin{enumerate}
        \item Construct a stack simulation for the word \(aabb\). This is a series of
        transitions leading to the acceptance of the word, showing the complete
        state of the stack at each step.
        \item Verify that at each step, the stack state represents exactly the
        remaining computation, in terms of the grammar \(G\). [open-ended] 
        \item Attempt to construct a stack simulation for the word \(aabbba\).
        What do you notice in analogy with the partial parse tree for this word?
        [open-ended]
    \end{enumerate}


    \begin{solution}

        \begin{enumerate}
            \item We represent a state and stack here as \([q, abcd]\) where
            \(q\) is the current state, and \(abcd\) the current stack contents,
            with \(a\) at the top. The following is the stack simulation for the
            word \(aabb\):
            
            \begin{center}
                \begin{tikzpicture}[node distance = 2cm, on grid]
                    \node (s0) {\([q_0, Z]\)};
                    \node[right = 3cm of s0] (s1) {\([q_1, SZ]\)};
                    \node[right = 3cm of s1] (s2) {\([q_1, aSBZ]\)};
                    \node[right = 3cm of s2] (s3) {\([q_1, SBZ]\)};
                    \node[below of = s3] (s4) {\([q_1, aSBBZ]\)};
                    \node[left  = 3cm of s4] (s5) {\([q_1, SBBZ]\)};
                    \node[left  = 3cm of s5] (s6) {\([q_1, BBZ]\)};
                    \node[left  = 3cm of s6] (s7) {\([q_1, BZ]\)};
                    \node[below of = s7] (s8) {\([q_1, Z]\)};
                    \node[right = 3cm of s8] (s9) {\([q_f, \epsilon]\)};
                    \node[right = 3cm of s9] (done) {Done \(\checkmark\)};

                    \path[->]
                    (s0) edge node[above] {\scriptsize \(\langle \epsilon, Z, SZ \rangle\)} (s1)
                    (s1) edge node[above] {\scriptsize \(\langle \epsilon, S, aSB \rangle\)} (s2)
                    (s2) edge node[above] {\scriptsize \(\langle        a, a, \epsilon \rangle\)} (s3)
                    (s3) edge node[left ] {\scriptsize \(\langle \epsilon, S, aSB \rangle\)} (s4)
                    (s4) edge node[above] {\scriptsize \(\langle        a, a, \epsilon \rangle\)} (s5)
                    (s5) edge node[above] {\scriptsize \(\langle \epsilon, S, \epsilon \rangle\)} (s6)
                    (s6) edge node[above] {\scriptsize \(\langle        b, B, \epsilon \rangle\)} (s7)
                    (s7) edge node[left ] {\scriptsize \(\langle        b, B, \epsilon \rangle\)} (s8)
                    (s8) edge node[above] {\scriptsize \(\langle \epsilon, Z, \epsilon \rangle\)} (s9)
                    ;
                    
                \end{tikzpicture}
            \end{center}

            \item This is true. While informal here, it can be formalized with
            the use of \emph{sentential forms}. Doing this formally is not hard,
            but technical and not in the scope of this course. You can find the
            details in \cite[section 7.2]{linz2022introduction}.
            \item Same as in the case of parse trees, symbols remain on the
            stack which cannot be elaborated finitely in a desired manner.
        \end{enumerate}
        
    \end{solution}
\end{exercise}