% lr.tex
% problems over LR parsing

\subsection*{LR Parsing}

We have seen left-to-right parsing of context-free grammars in class. However,
parsing languages in this way can be affected strongly by the way we express our
grammar. Consider the regular language \((a\mid b)^*ab\) of words ending with
\(ab\) represented as a CFG with rules:
%
\begin{align*}
    S &\to Sab \mid SS \mid a \mid b
\end{align*}

Convince yourself that this is indeed the same language. We can write a small
top-down parser for words in this language:

\begin{lstlisting}[language=scala]
enum State:
  case Failure
  case Success(remaining: List[Char])

import State.*

object S:
  val a = 'a'
  val b = 'b'
  def apply(word: List[Char]): Boolean =
    parse(word) match
      case Success(Nil) => true
      case _            => false

  def parse(word: List[Char]): State =
    parse(word) match
      case Failure =>
        if word == List(a) || word == List(b) then Success(Nil)
        else Failure
      case Success(remaining) =>
        parse(remaining) match // SS
          case Failure =>
            if remaining == List(a, b) then Success(Nil)
          case Success(Nil) => Success(Nil)
          case _ => Failure
        else Failure
\end{lstlisting}

Running it on a simple word \(babab\), we get:
%
\begin{lstlisting}[language=scala]
S("babab".toList) 
//> java.lang.StackOverflowError
\end{lstlisting}

It's clearly a small word, so what went wrong? The parser sees any word, and
attempts to `consume' a token matching \(S\). However, consuming a token
matching \(S\) requires consuming a token matching \(S\), ad infinitum!

% Now, from this CFG, we
% can generate a pushdown automaton representing a left-to-right parser for the
% language:
% %
% \begin{center}
%     \begin{tikzpicture}[node distance = 3cm, on grid]
%         \node[state, initial] (q0) {\(q_0\)};
%         \node[state, right=of q0, minimum size = 1.5cm] (q1) {\(q_1\)};
%         \node[state, accepting, right=of q1] (qf) {\(q_f\)};

%         \draw[->] (q0) edge node[above] {\scriptsize \(\langle \epsilon, Z, SZ\rangle\)} (q1);
%         \draw[->] (q1) edge node[above] {\scriptsize \(\langle \epsilon, Z, \epsilon\rangle\)} (qf);

%         \draw[->, out=45, in=135, loop] (q1.north) to node[above] {\scriptsize \(\langle \epsilon, S, B\rangle\)} (q1.north);
%         \draw[->, out=-90, in=0, loop] (q1.315) to node[below right] {\scriptsize \(\langle \epsilon, S, SAB\rangle\)} (q1.315);
%         \draw[->, out=180, in=270, loop] (q1.225) to node[below left] {\scriptsize \(\langle a, A, \epsilon\rangle\)} (q1.225);
%         \draw[->, out=225, in=315, loop] (q1.270) to node[below] {\scriptsize \(\langle b, B, \epsilon\rangle\)} (q1.270);
%     \end{tikzpicture}
% \end{center}

% \begin{exercise}{\easy}
%     For the word \(babab\), construct the stack simulation of the PDA above. Is
%     this the only
% \end{exercise}

However, hope is not lost! We can transform the grammar to remove the
left-recursion. Intuitively, the procedure involves replacing occurrences of the
left-recursion with right-recursion, by unfolding the rules one step, to switch
prefixes and suffixes during parsing.

The procedure is well described on
\href{https://en.wikipedia.org/wiki/Left_recursion#Removing_left_recursion}{Wikipedia
/ Left Recursion / Removing left recursion}. Here, we will describe it briefly
as we apply it to the small grammar above as an example:

\begin{enumerate}
    \item Starting with the grammar:
    %
    \begin{align*}
        S &\to Sab \mid SS \mid a \mid b
    \end{align*}
    %
    \item Remove all rules of the form \(A \to A\). (We have none.)
    \item Split the rules of the grammar into two for each non-terminal into
    left-recursive and others:
    %
    \begin{align*}
        S &\to S\alpha \mid SS \\
        S &\to \beta \text{ where \(\beta\) does not start with \(S\)}
    \end{align*}
    %
    We have the split rules \(S \to Sab \mid SS\) and \(S \to a \mid b\).
    \item Introduce a new non-terminal \(S'\) corresponding to `suffixes of
    \(S\)'. For each rule \(S \to S \alpha\):
    \begin{enumerate}
        \item Remove the rule \(S \to S \alpha\).
        \item Add the rule \(S' \to \alpha S'\).
        \item Add the rule \(S' \to \epsilon\).
    \end{enumerate}
    We add the rule \(S' \to abS' \mid SS' \mid \epsilon\).
    \item Finally, for each non-left-recursive rule \(S \to \beta\), add the
    right-recursive rule \(S \to \beta S'\). We add the rules \(S \to aS' \mid
    bS'\).
\end{enumerate}

Our grammar no longer has left-recursion!
%
\begin{align*}
    S &\to aS' \mid bS' \\
    S' &\to abS' \mid SS' \mid \epsilon
\end{align*}

This is sufficient here as we have \emph{direct} left-recursion. In general, we
may have to unfold rules to discover hidden or \emph{indirect} left recursion.
For each recursive rule, if a non-terminal occurs at the leftmost position, we
unfold it, till the grammar no longer changes. Finally, we apply the procedure
as above.

\begin{exercise}{\easy}
    For the following context-free grammars, produce the equivalent grammars
    without left-recursion:
    %
    \begin{enumerate}
        \item \(G_2\):
        %
        \begin{align*}
            List &\to [ListExpr] \\
            ListExpr &\to ListExpr, Expr \mid Expr
        \end{align*}
        %
        Assume that \(Expr\) has no internal recursion or references to
        \(List\).
        \item[\stepcounter{enumi}\hard\theenumi.]
        \(G_3\), for arithmetic expressions:
        \begin{align*}
            Expr &\to Term + Expr \mid Term \\
            Term &\to Factor * Term \mid Factor \\
            Factor &\to Number \mid (Expr)
        \end{align*}
    \end{enumerate}

    \begin{solution}
        \begin{enumerate}
            \item \(G_2\): the recursion is direct, and the usual algorithm applies:
            %
            \begin{align*}
                List &\to [ListExpr] \\
                ListExpr &\to Expr~ ListExpr' \\
                ListExpr' &\to~ , Expr~ ListExpr' \mid \epsilon
            \end{align*}
            %
            \item \(G_3\): this language has indirect recursion, so we first
            have to unroll and saturate the language. For example, In \(Term\),
            \(Factor\) occurs at the leftmost position, so we unroll first
            \(Factor\), and then \(Expr\) in \(Term\):
            %
            \begin{align*}
                1: && Term &\to Number * Term \mid (Expr) * Term \mid Number \mid Expr \\
                2: && Term &\to Number * Term \mid (Expr) * Term \mid Number \mid Term + Expr \mid Term \\
            \end{align*}
            %
            This is the fully unfolded form of \(Term\). (Assuming \(Number\) is
            a simple non-terminal not causing recursion). Now, the algorithm for
            removing direct recursion can be applied. We get:
            %
            \begin{align*}
                Term &\to Number * Term~ Term' \mid (Expr) * Term~ Term' \mid Number~ Term' \\
                Term' &\to + Expr~ Term' \mid \epsilon
            \end{align*}
            %
            \(Factor\) remains the same. Curiously, \(Expr\) also remains
            unchanged, all because of the parentheses around \(Expr\) in
            \(Factor\)! This is an interesting small insight into how extra
            parentheses can reduce ambiguity in the grammar and help parsing.
        \end{enumerate}
    \end{solution}
\end{exercise}
