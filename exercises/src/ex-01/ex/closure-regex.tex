% closure-regex.tex
% closure properties of regular expressions


\begin{exercise}{\easy}
    Consider a language \(L \subseteq \Sigma^*\) for a finite alphabet
    \(\Sigma^*\). Given an NFA \(A = (Q, \Sigma, \delta, S, F)\) whose language
    is \(L\), construct an NFA representing:
    %
    \begin{enumerate}
        \item \(L^2\), doubling every word in \(L\),
        \item \(L^n\), and
        \item \(L^*\).
    \end{enumerate}

    
    \begin{solution}
        For \(L^2\), consider the automaton composed of two copies of \(A\), say
        \(A_1\) and \(A_2\). Add \(\epsilon-\)edges from every final state of
        \(A_1\) to every initial state of \(A_2\).
        
        For \(L^n\), same as above, but with \(n\) copies of \(A\).

        \(L^*\) is simpler. In \(A\), draw an \(\epsilon-\)edge from every final
        state to every initial state. It is interesting but not too hard to
        argue that this automaton suffices.

        So, if \(L\) is regular, so are \(L^n\) for any \(n \in \naturals\) and
        \(L^*\). If we know how to parse one expression, we can parse any number
        of them in a sequence!

        The simplicity of the construction of \(L^*\) is somewhat expected,
        since you do not have to `remember' anything after a repetition, and can
        `memorylessly' repeat the language. This has interesting consequences
        (see exercise on characteristic functions below).
    \end{solution}   
    
\end{exercise}


\begin{exercise}{\hard}
    \label{ex:charfun}
    Characteristic functions and automata

    Remind yourself of the characteristic function(s) that we've seen in class.
    Each function definition is akin to a state in an automaton, which consumes
    one or more letters, and checks whether it can be recognized by another
    state, or reports success, like a final state.
    %
    \begin{lstlisting}[language=scala]
trait CharacteristicFunction[A]:
  def apply(w: Array[A], from: Int): Boolean
end CharacteristicFunction
    \end{lstlisting}
    
    However, in the framework of Scala, nothing stops us from maintaining more
    information between our function calls, or doing arbitrary computation.

    Write a characteristic function representing the language \(\{a^nb^n ~|~ n
    \in \naturals\}\) in Scala.


    \begin{solution}
        Extending the characteristic function interface using an additional
        internal state with a counter:

        \begin{lstlisting}[language=scala]
object AnBn extends CharacteristicFunction[Int]:
  val a = 'a'.toInt
  val b = 'b'.toInt
  
  sealed trait State:
    val num: Int

  // how many a's have we read
  case class ReadA(num: Int) extends State
  // how many more b's should we read
  case class ReadB(num: Int) extends State 
  
  def apply(w: Array[Int], from: Int): Boolean =
    def rec(w: Array[Int], from: Int, state: State): Boolean =
      if w.length == from then
        // finish reading, check if done
        state.num == 0
      else
        // peek and proceed counting
        val isA = w(from) == a
        val isB = w(from) == b
        state match
          case ReadA(num) => 
            if isB then 
              rec(w, from + 1, ReadB(num - 1)) 
            else if isA then 
              rec(w, from + 1, ReadA(num + 1)) 
            else false
          case ReadB(num) =>
            if isB then
              (num > 0) && rec(w, from + 1,  ReadB(num - 1))
            else false
    rec(w, from, ReadA(0))
end AnBn

AnBn(Array(97, 97, 97), 0)
// res0: Boolean = false
AnBn(Array(97, 97, 97, 98, 98), 0)
// res1: Boolean = false
AnBn(Array(97, 97, 97, 98, 98, 98), 0)
// res2: Boolean = true
AnBn(Array(), 0)
// res3: Boolean = true
        \end{lstlisting}
    \end{solution}

    The fact that this is possible shows us that Scala is more expressive than
    NFAs/DFAs! (interesting at least, even if not entirely surprising)

    Looking in the other direction, the finite automata characterize an
    interesting subset of Scala, where we maintained no extra information
    besides the state we are in, and where we are in the word. This gives us the
    interesting insight that automata characterize `memoryless' computations!

    This makes finite automata a restricted model of computation, in the way
    that Newton's laws form a restricted model of the physical world. Newton's
    laws cannot express or correctly predict \emph{every} physical phenomenon,
    but are nevertheless crucial to explain certain phenomena at the human
    scale. 
\end{exercise}


\begin{exercise}{\hard}
    [Note: open-ended]

    What languages can be represented by characteristic functions written in
    Scala or another powerful programming language? Can we represent \emph{any}
    arbitrary set of words?

    \begin{solution}
        Scala is Turing-complete (which just requires conditionals, loops, and
        memory), so it can express a function to recognize any set a Turing machine
        can. Even that isn't \emph{every} set.

        It is the class of \emph{recursively} or \emph{computably enumerable}
        sets, which are characterized by some partial computable function. If a
        set is not r.e., it is \emph{undecidable}.

        Even close to us, some undecidable problems can be found. Deciding
        whether two context-free grammars generate the same set of strings, for
        example, is undecidable! (can be transformed to ask whether the
        intersection of two CFGs is empty).
        
        Many other interesting examples can be found on the
        \href{https://en.wikipedia.org/wiki/List_of_undecidable_problems}{List
        of undecidable problems on Wikipedia}.
    \end{solution}
\end{exercise}


