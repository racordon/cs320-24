
\subsection*{Numerals}

In the previous section, we defined an encoding of Boolean values into the
Lambda calculus. Crucially, this encoding emphasizes the \emph{computational}
nature of the Booleans, rather than looking at them as ``just data''.

Similarly, one can question whether a computational meaning can be assigned to
numerals. In this vein, we define the following lambda terms
%
\begin{align*}
    c_0 &= \lambda s.\; \lambda z.\; z \\
    c_1 &= \lambda s.\; \lambda z.\; s ~z \\
    c_2 &= \lambda s.\; \lambda z.\; s ~(s ~z) \\
    \vdots
\end{align*}
%
where \(c_n\) accepts two arguments, \(s\) and \(z\), and applies \(s\) to \(z\)
successively \(n\) times. Intuitively, \(c_n\) captures the computational nature
of \(n\), the ability to count and ``do something'' \(n\) times. These are
called \(Church\) numerals, after Alonzo Church. We use \(c_n\) synonymously
with the natural number \(n\) herein. 

And by applying \(s\) ``once more'', we can define a successor function
%
\begin{align*}
    \lstinline|succ| = \lambda n.\; \lambda s.\; \lambda z.\; n ~s ~(s z)
\end{align*}

Given any natural number \(n\) encoded as a Church numeral, \(\lstinline|succ|
~n\) is another numeral (accepts \(s\) and \(z\), applies successively!), such
that it applies \(s\) to \(z\) exactly once more than \(n\) would.


\begin{exercise}{\easy}
    With the \lstinline|succ| function, and the ability to apply a function
    multiple times, define a function \lstinline|plus| on natural numbers
    encoding addition.

    \begin{solution}
        \(\lstinline|plus| = \lambda m.\; \lambda n.\; m ~\lstinline|succ| ~n\) 
    \end{solution}
\end{exercise}


\begin{exercise}{\easy}
    Akin to the function \lstinline|plus|, define the function \lstinline|mul|
    to perform multiplication on natural numbers as a lambda term.

    \begin{gather*}
        \lstinline|mul| = \lambda m.\; \lambda n.\; ???
    \end{gather*}

    \begin{solution}
        \begin{align*}
            \lstinline|mul| &= \lambda m.\; \lambda n.\; m ~(\lstinline|plus| ~n) ~0
        \end{align*}

        The first part constructs the expression \((n + (n + \ldots m
        \text{ times } + (n + \_)))\) and then the right side finally applies it to
        0.

        Alternatively, \(\lstinline|mul| = \lambda m.\; \lambda n.\;\lambda s.\; m ~(n ~s)\).
    \end{solution}
\end{exercise}

\begin{exercise}{\easy}
    Using \lstinline|flipT| (defined above), define a function \(P\) on numbers such
    that
    %
    \begin{gather*}
        P = \lambda n.\; n ~\lstinline|flipT| ~ \tru~.
    \end{gather*}
    %
    Given a number \(n\) encoded as above, what does the function \(P\) do?
    Discuss, and try it on some examples manually.

    \begin{solution}
        Attempting to evaluate it on a few examples:
        %
        \begin{align*}
            P ~0 &= 0 ~\lstinline|flipT| ~\tru = (\lambda s.\; \lambda z.\; z) ~\lstinline|flipT| ~\tru = \tru\\
            P ~1 &= 1 ~\lstinline|flipT| ~\tru = (\lambda s.\; \lambda z.\; s z) ~\lstinline|flipT| ~\tru = \lstinline|flipT| ~\tru = \fls\\
            P ~2 &= 2 ~\lstinline|flipT| ~\tru = (\lambda s.\; \lambda z.\; s (s z)) ~\lstinline|flipT| ~\tru \\ 
                    &= \lstinline|flipT| ~(\lstinline|flipT| ~\tru) = \fls
        \end{align*}

        Given a number \(n\), \(P\) checks if \(n = 0\). (alternatively \lstinline|isZero|)
    \end{solution}
\end{exercise}

\begin{exercise}{\easy}
    Define the function \lstinline|exp| on natural numbers
    % and \lstinline|leq| as Lambda terms. 
    as a lambda term.
    You may reuse the functions defined above. 

    What are the similarities or differences, if any, between \lstinline|plus|,
    \lstinline|mul|, and \lstinline|exp|?

    \begin{solution}
        \begin{align*}
            \lstinline|exp| &= \lambda m.\; \lambda n.\; m ~(\lstinline|mul| ~n) ~1
        \end{align*}

        \lstinline|exp|, \lstinline|mul|, and \lstinline|plus| all lift an
        existing operation by applying it multiple times. The operation being
        repeated affects the choice of the identity (final argument) in the
        expression, being \(0\) for addition and \(1\) for multiplication.

        Alternatively, \(\lstinline|exp| = \lambda m.\; \lambda n.\; n ~m\)~.

        % \begin{align*}
        %     \lstinline|leq| &= \lambda m.\; \lambda n.\; \lstinline|isZero| ~(\lstinline|minus| ~m ~n)
        % \end{align*}

        % Note that \lstinline|leq| takes advantage of the fact that our
        % subtraction stops at zero.
    \end{solution}
\end{exercise}
