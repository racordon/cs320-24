

Recall that in the previous exercise, we defined the function \lstinline|and|
\(= \lambda b_1.\; \lambda b_2. b_1 (b_2 ~\tru~\fls)~\fls\) for conjunction of
booleans. However, nothing stops us from writing \(\lstinline|and| ~1 ~2\). The
rewrite rules of lambda calculus would allow us to write and evaluate this term,
but intuitively, it has lost its meaning for us. We can prevent this by
asserting that arguments to \lstinline|and| must themselves be booleans, but we
have no formal definition of what booleans even are. This is made precise by
types.

As the basis for our discussion of types, we define a \emph{simply-typed lambda
calculus}.

First, we define a set of \emph{types}, given a set of \emph{type variables} \(V
= \{A, B, \ldots\}\), with the grammar:
%
\begin{align*}
    type &::= var \mid base \mid type \to type \\
    base &::= Bool
    var  &::= A \in V 
\end{align*}
%
where \(A \to B\) represents the arrow or function type, the set of functions
which take an argument of type \(A\), and produce a value of type \(B\). We
define two \emph{base types} that are fundamental to our system, 

With this, we can define the set of simply-typed lambda terms, given a set of
variables \(X = \{x, y, \ldots\}\):
%
\begin{align*}
    term &::= var \mid term ~term \mid \lambda var : type.\; term\\
    var &::= x \in X
\end{align*}

Note that compared to the untyped lambda calculus, we just added a type to the
abstraction.

% Recall that we defined \(\tru = \lambda m.\; \lambda n.\; m\) in the untyped
% system. Here, we can change that to
% %
% \begin{gather*}
%     \tru = \lambda m: A.\; \lambda n: A.\; m
% \end{gather*}
% %
% and define the type of booleans as \(Bool = A \to A \to A\) for any \(A\). This
% makes sure we cannot construct `bad' terms like the example above.


% \begin{exercise}{\easy}
%     In the definition of \tru, can we choose the types of \(m\) and \(n\) to be
%     different?
% \end{exercise}

Reasoning about the types in a program is no different from doing proofs. We
reason about well-formedness of types in the same way we reason about the
well-formedness of propositions, and prove equality of terms or show the type of
terms in the same way as we prove propositions. 

We can define a predicate \(x : A\), read \(x\) is of type \(A\), and provide
rules to reason about it!
%
\begin{gather*}
    \AxiomC{\(\Gamma, x : A \vdash t : B\)}
    \RightLabel{Abs}
    \UnaryInfC{\(\Gamma \vdash (\lambda x : A.\; t) : A \to B\) }
    \DisplayProof \\\\
    \AxiomC{\(\Gamma \vdash s : A \to B\)}
    \AxiomC{\(\Gamma \vdash t : A\)}
    \RightLabel{App}
    \BinaryInfC{\(\Gamma \vdash (s ~t) : B\) }
    \DisplayProof
\end{gather*}
%
Finally, we have to provide rules for each of our chosen base types, here just
\(Bool\):
\begin{align*}
    \AxiomC{}
    \RightLabel{TrueBool}
    \UnaryInfC{\(\tru : Bool\)} 
    \DisplayProof
    &&
    \AxiomC{}
    \RightLabel{FalseBool}
    \UnaryInfC{\(\fls : Bool\)}
    \DisplayProof
\end{align*}
\begin{gather*}
    \AxiomC{\(\Gamma \vdash b: Bool\)}
    \AxiomC{\(\Gamma \vdash m: T\)}
    \AxiomC{\(\Gamma \vdash n: T\)}
    \RightLabel{If-Then-Else (ITE)}
    \TrinaryInfC{\(\Gamma \vdash (b ~m ~n) : T\)}
    \DisplayProof
\end{gather*}
for any type \(A\).
%
However, nothing yet stops us from writing \(m : Boolean, m : Integer \vdash (m
~m ~m) : Integer\), so we add the restriction that a context \(\Gamma\) can only
contain \emph{one} typing for a given term.
% \begin{gather*}
%     \AxiomC{\(\Gamma \vdash t : B\)}
%     \AxiomC{\(\Gamma \vdash t : A\)}
%     \RightLabel{UniqueType}
%     \BinaryInfC{\(\Gamma \vdash A = B\) }
%     \DisplayProof
% \end{gather*} 

We say that a closed term \(t\) has the type \(A\) if there exists a derivation
of \(\vdash t : A\).


\begin{exercise}{\easy}
    Prove that the term \lstinline|and| \(= \lambda b_1 : Bool.\; \lambda b_2 :
    Bool.\; b_1 ~(b_2 ~\tru ~\fls) ~\fls\) has the type \(Bool \to Bool \to
    Bool\).


    \begin{solution}
        \begin{gather*}
            \AxiomC{}
            \RightLabel{Hyp}
            \UnaryInfC{\(b_2 : Bool \vdash b_2 : Bool\)}
            \AxiomC{}
            \RightLabel{TrueBool}
            \UnaryInfC{\(\vdash \tru : Bool\)}
            \RightLabel{Weakening}
            \UnaryInfC{\(b_2 : Bool \vdash \tru : Bool\)}
            \AxiomC{}
            \RightLabel{FalseBool}
            \UnaryInfC{\(\vdash \fls : Bool\)}
            \RightLabel{Weakening}
            \UnaryInfC{\(b_2 : Bool \vdash \fls : Bool\)}
            \RightLabel{ITE}
            \TrinaryInfC{\(b_2 : Bool \vdash (b_2 ~\tru ~\fls) : Bool\)}
            \DisplayProof
        \end{gather*}

        Another appropriate application of ITE, followed by two applications of
        Abs give the required derivation. We do not have space to fit it here.
    \end{solution}
\end{exercise}


\begin{exercise}{\hard}
    Argue that the following type ascription is \emph{incorrect}: \(\lambda x: A. x : Bool \to Bool\).

    \begin{solution}
        We only provide a brief sketch of the proof. The proof inducts on the
        size of the proof of type ascription of the given term, showing that the
        given ascription has no possible derivation.
        
        This is much harder that showing that a term \emph{does} have a given
        type! Fortunately, a compiler rarely has to worry about whether a term
        is \emph{not} of a given type, and as we will see later in the course,
        just being able to infer the types of terms is enough for most things we
        would like to do with types.
    \end{solution}
\end{exercise}

\begin{exercise}{\hard}
    Is it possible to define the Church numerals as simply typed lambda terms?
    Argue as to why / why not.


    \begin{solution}
        It is not possible to define them as is. \tru and \fls can be looked at
        as simpler examples as well.

        The Church numeral 0 already shows us the problem: \(\lambda s : A.\;
        \lambda z : B. z\). What do we write in place of \(A\) and \(B\)? We
        cannot leave them free, as 0 is a closed term, so its type must have a
        derivation ending in \(vdash 0 : T\) for some type \(T\).

        The type of 0 must be \(A \to B \to B\) for \emph{any} \(A\) and \(B\).
        Convince yourself that the type of all other Church numerals must be
        \((A \to A) \to A \to A\) for any \(A\).

        The type of the Church numerals is \emph{polymorphic} or \emph{generic}
        in \(A\), and cannot be expressed in our simple type system. More
        complicated type systems, like that of Scala, allow reasoning about
        these types just like our simple types.
    \end{solution}
\end{exercise}
