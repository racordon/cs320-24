
Consider a proof system of \emph{natural deduction} defined by the following
inference rules:
%
For hypotheses:
\begin{align*}
    \AxiomC{}
    \RightLabel{Hyp}
    \UnaryInfC{\(\Gamma, A \vdash A\)}
    \DisplayProof &&
    \AxiomC{\(\Gamma \vdash B\)}
    \RightLabel{Weakening}
    \UnaryInfC{\(\Delta \vdash B\)}
    \DisplayProof
\end{align*}
%
where \(\Gamma\) is contained in \(\Delta\) as a list.\\
For disjunction:
%
\begin{align*}
    \AxiomC{\(\Gamma \vdash A\)}
    \RightLabel{\(\lor-\text{intro}_1\)}
    \UnaryInfC{\(\Gamma \vdash A \lor B\)}
    \DisplayProof &&
    \AxiomC{\(\Gamma \vdash A\)}
    \RightLabel{\(\lor-\text{intro}_2\)}
    \UnaryInfC{\(\Gamma \vdash B \lor A\)}
    \DisplayProof
\end{align*}
\begin{align*}
    \AxiomC{\(\Gamma \vdash A \lor B\)}
    \AxiomC{\(\Gamma, A \vdash C\)}
    \AxiomC{\(\Gamma, B \vdash C\)}
    \RightLabel{\(\lor-\text{elim}\)}
    \TrinaryInfC{\(\Gamma \vdash C\)}
    \DisplayProof
\end{align*}
%
For conjunction:
\begin{align*}
    \AxiomC{\(\Gamma \vdash A\)}
    \AxiomC{\(\Gamma \vdash B\)}
    \RightLabel{\(\land-\text{intro}\)}
    \BinaryInfC{\(\Gamma \vdash A \land B\)}
    \DisplayProof
\end{align*}
%
\begin{align*}
    \AxiomC{\(\Gamma \vdash A \land B\)}
    \RightLabel{\(\land-\text{elim}_1\)}
    \UnaryInfC{\(\Gamma \vdash A \)}
    \DisplayProof &&
    \AxiomC{\(\Gamma \vdash A \land B\)}
    \RightLabel{\(\land-\text{elim}_2\)}
    \UnaryInfC{\(\Gamma \vdash B\)}
    \DisplayProof
\end{align*}
%
For implication:
%
\begin{align*}
    \AxiomC{\(\Gamma, A \vdash B\)}
    \RightLabel{\(\implies-\text{intro}\)}
    \UnaryInfC{\(\Gamma \vdash A \implies B\)}
    \DisplayProof
\end{align*}
%
\begin{align*}
    \AxiomC{\(\Gamma \vdash A \implies B\)}
    \RightLabel{\(\implies\)-elim}
    \UnaryInfC{\(\Gamma, A \vdash B\)}
    \DisplayProof &&
    \AxiomC{\(\Gamma \vdash A\)}
    \AxiomC{\(\Gamma \vdash A \implies B\)}
    \RightLabel{Modus-Ponens (MP)}
    \BinaryInfC{\(\Gamma \vdash B\)}
    \DisplayProof
\end{align*}
%
For quantifiers:
%
\begin{align*}
    \AxiomC{\(\Gamma \vdash A\)}
    \RightLabel{\(\forall-\text{intro}\)}
    \UnaryInfC{\(\Gamma \vdash \forall\, x.\; A\)}
    \DisplayProof
\end{align*}
%
where \(x\) is not free in \(\Gamma\), and
\begin{align*}
    \AxiomC{\(\Gamma \vdash A[x := t]\)}
    \RightLabel{\(\exists-\text{intro}\)}
    \UnaryInfC{\(\Gamma \vdash \exists\, x.\; A[x := x]\)}
    \DisplayProof
\end{align*}
for some term \(t\), with \(A[x := t]\) representing a capture-avoiding
substitution of \(x\) by \(t\) in \(A\), and for elimination:
%
\begin{gather*}
    \AxiomC{\(\Gamma \vdash \forall\, x.\; A\)}
    \RightLabel{\(\forall-\text{elim}\)}
    \UnaryInfC{\(\Gamma \vdash A[x := t]\)}
    \DisplayProof \\
    \AxiomC{\(\Gamma \vdash \exists\, x.\; A\)}
    \AxiomC{\(\Gamma, A \vdash B\)}
    \RightLabel{\(\forall-\text{elim}\)}
    \BinaryInfC{\(\Gamma \vdash B\)}
    \DisplayProof 
\end{gather*}
%
where \(x\) is not free in \(B\).

Optionally, if our system is agnostic to the order of the hypotheses, we can add
the rearranging rule:
%
\begin{gather*}
    \AxiomC{\(\Gamma \vdash A\)}
    \RightLabel{Rearrange}
    \UnaryInfC{\(\Gamma' \vdash A\)}
    \DisplayProof
\end{gather*}
%
where \(\Gamma'\) is a permutation of \(\Gamma\). You may use this rule unless
specified. We will see for the case of type systems how the absence of this rule
can be interesting.

Natural deduction, sequent calculus, as well as the term sequent itself, were
introduced by Gerhard Gentzen
\cite{gentzen1935untersuchungen,gentzen1935untersuchungen2}. There is also the
comprehensive page \href{https://en.wikipedia.org/wiki/Sequent_calculus}{Sequent
Calculus on Wikipedia}.

A set of inference rules resulting in the sequent \(\Gamma \vdash A\) is called
a \emph{derivation} of \(\Gamma \vdash A\).

Consider as an example, the following derivation of \(A \land B \vdash A \lor
B\):
%
\begin{gather*}
    \AxiomC{}
    \RightLabel{Hyp}
    \UnaryInfC{\(A \land B \vdash A \land B\)}
    \RightLabel{\(\land\)-elim\(_1\)}
    \UnaryInfC{\(A \land B \vdash A\)}
    \RightLabel{\(\lor\)-intro\(_1\)}
    \UnaryInfC{\(A \land B \vdash A \lor B\)}
    \DisplayProof
\end{gather*} 


\begin{exercise}{\easy}
    Produce derivations for the following:
    %
    \begin{enumerate}
        \item \(\forall x.\; P(x) \vdash \exists\, x.\; P(x)\)
        \item Fixed point application: \(\forall x.\; P(x) \implies P(f(x)) \vdash \forall x.\; P(x) \implies P(f(f(x)))\)
    \end{enumerate}


    \begin{solution}
        \begin{enumerate}
            \item \(\forall x.\; P(x) \vdash \exists\, x.\; P(x)\) 
            \begin{gather*}
                \AxiomC{}
                \RightLabel{Hyp}
                \UnaryInfC{\(\forall x.\; P(x) \vdash \forall x.\; P(x)\)}
                \RightLabel{\(\forall\)-elim}
                \UnaryInfC{\(\forall x.\; P(x) \vdash P(x)\)}
                \RightLabel{\(\exists\)-intro}
                \UnaryInfC{\(\forall x.\; P(x) \vdash \exists x.\; P(x)\)}
                \DisplayProof
            \end{gather*}
            \item \(\forall x.\; P(x) \implies P(f(x)) \vdash \forall x.\; P(x) \implies P(f(f(x)))\)\\
            First we separately prove the transitivity of \(implies\), \(P
            \implies Q, Q \implies R \vdash P \implies R\), as a \emph{deduced
            rule}:
            \begin{gather*}
                \AxiomC{\(\vdash P \implies Q\)}
                \RightLabel{\(\implies\)-elim}
                \UnaryInfC{\(P \vdash Q\)}
                %
                \AxiomC{\(\vdash Q \implies R\)}
                \RightLabel{MP}
                \BinaryInfC{\(P \vdash R\)}
                \RightLabel{\(\implies\)-intro}
                \UnaryInfC{\(\vdash P \implies R\)}
                \DisplayProof
            \end{gather*}
            and write it as
            \begin{gather*}
                \AxiomC{\(\Gamma \vdash P \implies Q\)}
                \AxiomC{\(\Gamma \vdash Q \implies R\)}
                \RightLabel{\(\implies\)-trans}
                \BinaryInfC{\(\Gamma \vdash P \implies R\)}
                \DisplayProof
            \end{gather*}
            For the final proof, we must define \(A = \forall x.\; P(x) \implies
            P(f(x))\) due to space constraints:
            \begin{gather*}
                \AxiomC{}
                \RightLabel{Hyp}
                \UnaryInfC{\(A \vdash \forall x.\; P(x) \implies P(f(x))\)}
                \RightLabel{\(\forall\)-elim}
                \UnaryInfC{\(A \vdash P(x) \implies P(f(x))\)}
                %
                \AxiomC{}
                \RightLabel{Hyp}
                \UnaryInfC{\(A \vdash \forall x.\; P(x) \implies P(f(x))\)}
                \RightLabel{\(\forall\)-elim}
                \UnaryInfC{\(A \vdash P(f(x)) \implies P(f(f(x)))\)}
                %
                \RightLabel{\(\implies\)-trans}
                \BinaryInfC{\(A \vdash P(x) \implies P(f(f(x)))\)}
                \RightLabel{\(\forall\)-intro}
                \UnaryInfC{\(A \vdash \forall x.\; P(x) \implies P(f(f(x)))\)}
                \DisplayProof
            \end{gather*}
        \end{enumerate}
    \end{solution}
\end{exercise}
