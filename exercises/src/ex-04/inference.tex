% inference.tex
% exercises on type inference

We say a type \(T\) for a term \(t\) is \emph{principal} if for any other type
\(T'\) such that \(t : T'\), then there must exist a substitution of type
variables \(\delta\) such that \(T\delta = T'\). Note that this is nearly
identical to the definition of a most-general unifier (above)!

Herein, we assume that are we working with the ``Lambda calculus + constants +
types'' system discussed in the lectures:

\begin{center}
    \centering
    \begin{tabular}[ht]{c c}
        \AxiomC{\(\Gamma \vdash e_1 : \tau \to \sigma\)}
        \AxiomC{\(\Gamma \vdash e_2 : \tau\)}
        \RightLabel{TApp}
        \BinaryInfC{\(\Gamma \vdash e_1~e_2 : \sigma\)} 
        \DisplayProof
        %
        &
        %
        \AxiomC{\(\Gamma, x : \tau \vdash e : \sigma\)}
        \RightLabel{TAbs}
        \UnaryInfC{\(\Gamma \vdash (\lambda\; x : \tau.\; e) : \tau \to \sigma\)} 
        \DisplayProof
        \\ \\
        
        \AxiomC{\(x : \tau \in \Gamma\)}
        \RightLabel{TVar}
        \UnaryInfC{\(\Gamma \vdash x : \tau\)}
        \DisplayProof
        %
        &
        %
        \AxiomC{\(c : \tau \in \Delta\)}
        \RightLabel{TCst}
        \UnaryInfC{\(\Gamma \vdash c : \tau\)}
        \DisplayProof
    \end{tabular}
\end{center}

The constant types \(\Delta\) contain the types of each natural number \(n :
\naturals\), the function \(< : \naturals \to \naturals \to \booleans\), the
booleans \(true : \booleans\) and \(false : \booleans\), and propositional
functions \(and: \booleans \to \booleans \to \booleans\), \(or: \booleans \to
\booleans \to \booleans\), and \(not: \booleans \to \booleans\).


\begin{exercise}{\easy}
    For each of the following lambda terms, compute a principal type,
    substituting type variables if required:
    %
    \begin{enumerate}
        \item \((\lambda\; x : X.\; \lambda y : Y.\; (< ~y ~x) ~y ~ x) ~4 ~2\)
        \item \(\lambda\; x : X. \; \lambda\; y : Y.\; and ~(< ~y ~x) ~(< ~x ~y)\)
        \item \(\lambda\; x : X.\; \lambda\; y : Y.\; \lambda\; z : Z.\; (x ~z) ~(y ~z)\)
        \note{\cite[Exercise 22.5.2]{pierce2002types}}
    \end{enumerate}


    \begin{solution}
        \begin{enumerate}
            \item the term has a single type \(\naturals\). 
            \item the term has a single type \(\naturals \to \naturals \to
            \booleans\). Note that this is the function \(= ~x ~y\) on the
            natural numbers.
            \item The term has principal type \((Z \to(Z \to B) \to A) \to (Z
            \to B) \to Z \to A\) for type variables \(A\), \(B\), and \(Z\).
            %
            From the last term, \(y\) must have type \(Z \to B\) for a type
            variable \(B\). Then, \((x ~z)\) must have type \((Z \to B) \to A\)
            for some type variable \(A\). Finally, \(x\) must have type \(Z \to
            (Z \to B) \to A\). The type of the lambda term is computed by
            abstraction. 
        \end{enumerate}

        The last part is discussed in more detail below.
    \end{solution}
\end{exercise}

% formalize constraint generation

Let us look at type inference as a uniform procedure on the term and the type
system. In a way, type inference is just looking at the typing rules
upside-down, trying to look for a typing derivation.

Given a term application \((t ~s)\), it must have type \(B\) for some type
\(B\). Looking at the rule TApp upside down, we get \(t : A \to B\) and \(s :
A\) for some type \(A\), which we can then process recursively.

Consider a term \((x ~y) ~(y ~z)\) (try the exercise above first!). Following
the typing rules in reverse, we see that this term must have a type \(B\), with
\((x ~y): A \to B\) and \((y ~z): A\). From the left, following the typing rules
again, we find that \(x : C \to (A \to B)\) and \(y : C\) for some type variable
\(C\). However, from the right, we see that \(z : D\) for some type variable
\(D\) and \(y : D \to A\). 

Now we have \emph{two} types for \(y\)! \(C\) and \(D \to A\)! However, it is
unreasonable for a term to have more than one type here. Looking back, we were
trying to find a \emph{principal} type for each term in the expression, and it
just so happens that one side produces more general information than the other.

We already know of a way to make two terms equal by adding more information to
variables! We can unify these two types to get a type for \(y\) that all
branches agree on.

\begin{exercise}{\easy}
    What do we deduce if we learn that \(y : T\) and \(y : S\) for types \(T\)
    and \(S\) from different parts of a term?

    \begin{solution}
        The term cannot be well-typed! For example: \(\lambda\; x: \naturals.\;
        \lambda\; z: \booleans. (y ~x) ~(y ~z)\).        
    \end{solution}
\end{exercise}

Looking at an entire term, there may be many terms to unify! So, we can
accumulate and look at them together in a procedure:
%
\begin{enumerate}
    \item To each variable in the term, assign a type variable such that \(x :
    X\).
    \item Looking at a term \(t\), assign a type variable \(T\) such that \(t :
    T\), and look at the typing rule for the term upside-down. Each of the
    premises generates a recursive constraint. For example: if \(t = (s ~u)\),
    then we get \(t : T\), and a constraint \(T = S \to U\) with \(s : S\) and
    \(u : U\). Terms \(s\) and \(u\) are processed recursively.
    \item If a recursive term is a variable, for example \(s = x : S\) above,
    produce the constraint \(X = S\).
    \item Finally, we unify each pair simultaneously to get the value for \(T\),
    and return it. This follows the same \lstinline|foldLeft| procedure as for
    recursive calls in term unification.
\end{enumerate}

% exercise to compute constraints, and then unify them on a nice term
\begin{exercise}{\easy}
    Consider the term \(t = \lambda\; x : X.\; \lambda\; y : Y.\; \lambda\; z :
    Z.\; (x ~z) ~(y ~z)\). For typing this term:
    %
    \begin{enumerate}
        \item What is the entire set of generated typing constraints?
        \item What is the mgu satisfying this set of constraints?
        \item What is the principal type of this term?
    \end{enumerate}


    \begin{solution}
        For readability, we label each subterm and assign type variables:
        %
        \begin{itemize}
            \item \(t : T = \lambda\; x : X.\; \lambda\; y : Y.\; \lambda\; z : Z.\; (x ~z) ~(y ~z)\)
            \item \(t_1 : T_1 = \lambda\; y : Y.\; \lambda\; z : Z.\; (x ~z) ~(y ~z)\)
            \item \(t_2 : T_2 = \lambda\; z : Z.\; (x ~z) ~(y ~z)\)
            \item \(t_3 : T_3 = (x ~z) ~(y ~z)\)
            \item \(t_4 : T_4 = (x ~z)\)
            \item \(t_5 : T_5 = x\)
            \item \(t_6 : T_6 = z\)
            \item \(t_7 : T_7 = (y ~z)\)
            \item \(t_8 : T_8 = y\)
            \item \(t_9 : T_9 = z\)
        \end{itemize}
        %
        For each labelled term, we look at the typing relation and generate constraints:
        %
        \begin{itemize}
            \item \(t_{\phantom{1}}: T_{\phantom{1}}\implies T = X \to T_1\)
            \item \(t_1 : T_1 \implies T_1 = Y \to T_2\)
            \item \(t_2 : T_2 \implies T_2 = Z \to T_3\)
            \item \(t_3 : T_3 \implies T_3 = A, T_4 = T_7 \to A\)
            \item \(t_4 : T_4 \implies T_4 = B, T_5 = T_6 \to B\)
            \item \(t_5 : T_5 \implies T_5 = X\)
            \item \(t_6 : T_6 \implies T_6 = Z\)
            \item \(t_7 : T_7 \implies T_7 = C, T_8 = T_9 \to C\)
            \item \(t_8 : T_8 \implies T_8 = Y\)
            \item \(t_9 : T_9 \implies T_9 = Z\)
        \end{itemize}
        %
        The right-hand side is the set of generated constraints.
        %
        The unifier assigns the substitutions:
        %
        \begin{itemize}
            \item \(T  ~\mapsto (Z \to C \to A) \to (Z \to C) \to Z \to A\)
            \item \(T_1 \mapsto (Z \to C) \to Z \to A\)
            \item \(T_2 \mapsto Z \to A\)
            \item \(T_3 \mapsto A\)
            \item \(T_4 \mapsto C \to A\)
            \item \(T_5 \mapsto Z \to C \to A\)
            \item \(T_6 \mapsto Z\)
            \item \(T_7 \mapsto C\)
            \item \(T_8 \mapsto Z \to C\)
            \item \(T_9 \mapsto Z\)
        \end{itemize}

        The assigned type \(T\) is the final principal type of \(t\).

    \end{solution}
\end{exercise}


\begin{exercise}{\hard}
    Compute the principal type of the term \(Y = (\lambda\; x.\; x ~x) (\lambda\; x.\; x ~x)\).

    \begin{solution}
        The term is not well-typed. Unification of constraints fails.
    \end{solution}
\end{exercise}

