% unification.tex
% exercises on first-order  term unification

\begin{exercise}{\easy}
    \label{ex:findunifier}
    Are the following sets of terms unifiable? \(x, y, z\) are variables.
    %
    \begin{enumerate}
        \item \(f ~x ~y, f ~y ~x\)
        \item \(x, y, z\)
        \item \(f ~x ~(g ~a ~b), f ~(g ~a ~b) ~x\)
        \item \(f ~x ~(g ~a ~b), f ~(g ~c ~d) ~x\)
    \end{enumerate}


    \begin{solution}
        \begin{enumerate}
            \item Yes, \(\sigma = \{x \mapsto y\}\).
            \item Yes, \(\sigma = \{x \mapsto z, y \mapsto z\}\)
            \item Yes, \(\sigma = \{x \mapsto g ~ a ~ b\}\)
            \item No. To be unifiable, the terms must be equal after a single,
            common substitution. For the LHS to unify, we must have \(x \mapsto
            g ~c ~d\), but for the RHS we must have \(x \mapsto g ~a ~b\). The
            two are incompatible. 
        \end{enumerate}
    \end{solution}
    
\end{exercise}


\begin{exercise}{\easy}
    For each of the sets of terms in the previous exercise for which a unifier
    exists, is the unifier unique? Is there a principal or ``most-general''
    unifier?
    
    \begin{solution}
        \begin{enumerate}
            \item \(f ~x ~y, f ~y ~x\): Not unique. \(\sigma = {x \mapsto a, y \mapsto a}\) is another example.
            \item \(x, y, z\): Not unique. \(\sigma = {x \mapsto a, y \mapsto a, z \mapsto a}\) is another example.
            \item \(f ~x ~(g ~a ~b), f ~(g ~a ~b) ~x\): unique. (why?)
            \item \(f ~x ~(g ~a ~b), f ~(g ~c ~d) ~x\): No unifier.
        \end{enumerate}

        For both of the non-unique cases, note that the provided substitutions
        are ``less general'' the ones in the solutions for the previous
        exercise. Some variables are further instantiated to constants, or some
        variables that were not previously required to be substituted are
        instantiated now.

        Computing principal / most-general unifiers allows us to preserve the
        most amount of information possible. 
    \end{solution}
\end{exercise}

A unifying substitution for a set of terms, say \(\sigma\), is said to be a
\emph{most-general unifier} (mgu) if for any other unifier \(\sigma'\), there
exists another substitution \(\delta\) such that \(t\sigma\delta = t\sigma'\)
for all terms \(t\).

Intuitively, this means that \(\sigma'\) is obtained from \(\sigma\) by
substituting and instantiating more variables than is necessary. Note that
most-general unifiers are not necessarily unique! Variables can be renamed
to make ``equally general'' unifiers. As an example, the set of terms \(\{f
~x ~y, f ~y ~x\}\) has the most general unifiers \(\sigma_1 = \{x \mapsto
y\}\) as well as \(\sigma_2 = \{y \mapsto x\}\). 

\begin{exercise}{\hard}
    Show that the unification algorithm in the preliminaries computes a unifier
    for any pair of terms if one exists. Optionally, generalize it to unify a
    set of terms. Argue that this unifier is an mgu (open; proof is not given
    here).

    \begin{solution}
        The algorithm is recursive, so it only makes sense to do an inductive
        proof.

        Consider that our unifier works takes two terms, \(t\) and \(s\), and
        computes a unifier \(\sigma\). Without loss of generality, assume that
        \(t\) is the smaller term of the two. We can flip the recursive call if
        not. (Is the unifier of \((t, s)\) always a unifier for \((s, t)\)?)

        Base case: \(t = x\) is a variable. \(\sigma = \{x \mapsto s\}\) is the mgu.

        Inductive case: \(t = f(t_1, t_2, \ldots, t_n)\). Since \(t\) and \(s\)
        are assumed to be unifiable, it must be the case that \(s = f(s_1, s_2,
        \ldots, s_n)\) for some terms \(s_1, \ldots, s_n\).

        The recursive calls successively compute the unifiers \(\sigma_1,
        \sigma_2, \ldots, \sigma_n\) for the pairs \((t_1, s_1), (t_2, s_2),
        \ldots, (t_n, s_n)\). By the construction using \lstinline|foldLeft|, we
        have that \(\sigma_i\) is more general than \(\sigma_j\) if \(i \le j\).

        Using this, finally, we get \( f(t_1\sigma_n, t_2\sigma_n, \ldots,
        t_n\sigma_n) = f(s_1\sigma_n, s_2\sigma_n, \ldots, s_n\sigma_n)\). But
        by the definition of substitution, this means that \(f(t_1, t_2, \ldots,
        t_n)\sigma_n = f(s_1, s_2, \ldots, s_n)\sigma_n\), or \(t\sigma_n =
        s\sigma_n\).
    \end{solution}
\end{exercise}

It is crucial to type inference algorithms that we compute most-general unifiers
where possible. If not, type information can be lost that could have been used
to unify and type larger programs.

